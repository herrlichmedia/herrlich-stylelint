module.exports = {
    "rules": {
        /* Block rules */
        "block-closing-brace-empty-line-before": "never",
        "block-closing-brace-newline-after": "always",
        "block-closing-brace-newline-before": "always",
        "block-no-empty": true,
        "block-opening-brace-newline-after": "always",
        "block-opening-brace-space-before": "always",

        /* At rules */
        "at-rule-empty-line-before": ["always", { "ignore":  ["after-comment", "first-nested", "blockless-after-blockless"] }],
        "at-rule-name-newline-after": "always-multi-line",
        "at-rule-name-space-after": "always",
        "at-rule-no-vendor-prefix": true,
        "at-rule-semicolon-newline-after": "always",
        "at-rule-semicolon-space-before": "never",

        /* Declaration */
        "declaration-bang-space-after": "never",
        "declaration-bang-space-before": "always",
        "declaration-block-no-redundant-longhand-properties": true,
        "declaration-block-semicolon-newline-after": "always",
        "declaration-block-semicolon-newline-before": "never-multi-line",
        "declaration-block-semicolon-space-before": "never",
        "declaration-block-single-line-max-declarations": 1,
        "declaration-block-trailing-semicolon": "always",
        "declaration-colon-space-after": "always",
        "declaration-colon-space-before": "never",

        /* Media rules */
        "media-feature-colon-space-after": "always",
        "media-feature-colon-space-before": "never",
        "media-feature-name-no-vendor-prefix": true,
        "media-feature-parentheses-space-inside": "never",
        "media-feature-range-operator-space-after": "always",
        "media-feature-range-operator-space-before": "always",

        /* Selector rules */
        "selector-attribute-operator-space-after": "never",
        "selector-attribute-operator-space-before": "never",
        "selector-attribute-quotes": "always",
        "selector-combinator-space-after": "always",
        "selector-combinator-space-before": "always",
        "selector-descendant-combinator-no-non-space": true,
        "selector-max-empty-lines": 0,
        "selector-no-vendor-prefix": true,
        "selector-pseudo-class-case": "lower",
        "selector-pseudo-class-no-unknown": true,
        "selector-pseudo-class-parentheses-space-inside": "never",
        "selector-pseudo-element-case": "lower",
        "selector-type-case": "lower",
        "shorthand-property-no-redundant-values": true,

        /* Value rules */
        "value-keyword-case": "lower",
        "value-list-comma-space-after": "always-single-line",
        "value-list-comma-space-before": "never",
        "value-list-max-empty-lines": 0,
        "value-no-vendor-prefix": true,

        /* Number rules */
        "number-leading-zero": "never",
        "number-no-trailing-zeros": true,

        /* Property rules */
        "property-case": "lower",
        "property-no-unknown": true,
        "property-no-vendor-prefix": true,

        /* Unit rules */
        "unit-case": "lower",
        "unit-no-unknown": true,

        /* length */
        "length-zero-no-unit": true,

        /* General */
        "max-line-length": 120,
        "indentation": 4,
        "no-duplicate-at-import-rules": true,
        "no-duplicate-selectors": true,
        "no-empty-first-line": true,
        "no-extra-semicolons": true,
        "no-unknown-animations": true,
        "rule-empty-line-before": ["always", { "ignore":  ["first-nested", "after-comment"] }],
        "string-quotes": "single"
    }
};
