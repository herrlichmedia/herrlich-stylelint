# herrlich media stylelint config
> [stylelint](https://stylelint.io/) config

## Änderungen
Bei Änderungen der Config immer das Tag updaten und die Version hochzählen

## Installation
    
```bash
$ npm install --save-dev stylelint git+ssh://bitbucket.org:herrlichmedia/herrlich-stylelint.git
```

or with yarn

```bash
$ yarn add stylelint git+ssh://bitbucket.org:herrlichmedia/herrlich-stylelint.git --dev
```

## Usage

Add stylelint config to your package.json:

```json
{
    "stylelint": {
        "extends": "herrliches-stylelint"
    }
}
```
